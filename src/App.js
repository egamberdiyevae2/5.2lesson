// !!!! Task

// import React from "react";
// // import User from "./user";
// import "./app.css";

// export default class App extends React.Component {
//   UNSAFE_componentWillMount() {
//     console.log("will mount");
//   }
//   componentDidMount() {
//     console.log("did mount");
//   }

//   state = {
//     show: true,
//     name: "kim",
//   };
//   // componentDidUpdate() {
//   //   this.setState((state) => (state.name = "no"));
//   // }

//   add = () => {
//     this.setState((state) => (state.show = true));
//   };
//   remove = () => {
//     this.setState((state) => (state.show = false));
//   };

//   render() {
//     return (
//       <div
//         style={{
//           display: "flex",
//           alignItems: "center",
//           justifyContent: "center",
//           gap: "100px",
//           border: "1px solid black",
//           padding: "20px 30px",
//           width: "500px",
//           borderRadius: "15px",
//         }}
//       >
//         <div
//           style={{
//             display: "flex",
//             flexDirection: "column",
//             alignItems: "center",
//             justifyContent: "center",
//             gap: "100px",
//           }}
//         >
//           <button onClick={this.add}>Show</button>
//           <button onClick={this.remove}>Hide</button>
//         </div>
//         {this.state.show ? (
//           <div
//             style={{
//               display: "flex",
//               flexDirection: "column",
//               alignItems: "center",
//               justifyContent: "flex-start",
//               border: "2px solid green",
//               width: "300px",
//               height: "300px",
//               borderRadius: "15px",
//             }}
//           >
//             <h1
//               style={{
//                 color: "greenyellow",
//               }}
//             >
//               My Component
//             </h1>
//           </div>
//         ) : (
//           ""
//         )}
//       </div>
//     );
//   }
//   // componentDidMount() {
//   //   console.log("did mount");
//   // }
// }

import React from "react";
import { Route, Routes } from "react-router-dom";
import Nav from "./components/nav";
import Not from "./pages/Not";
import About from "./pages/about";
import Contact from "./pages/contact";
import Home from "./pages/home";
import Price from "./pages/price";
import Product from "./pages/product";
import "./app.css";

function App() {
  return (
    <div className="App">
      <Nav />
      <Routes>
        <Route path="/">
          <Route index element={<Home />} />
          <Route path="about">
            <Route index element={<About />} />
            <Route path="/about/one" element={"One"} />
            <Route path="/about/one" element={"Two"} />
          </Route>

          <Route path="product" element={<Product />} />
          <Route path="price" element={<Price />} />
          <Route path="contact" element={<Contact />} />
        </Route>
        <Route path="*" element={<Not />} />

        {/* <Home />
      <About />
      <Price />
      <Product />
      <Contact /> */}
      </Routes>
    </div>
  );
}

export default App;
