import React, { Component } from "react";
import {  Link } from "react-router-dom";

export default class nav extends Component {
  render() {
    return (
      <div>
        <nav
          className="nav"
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            gap: "30px",
          }}
        >
          <Link className="btn btn-primary m-5" to="/">
            Home
          </Link>
          <Link className="btn btn-primary m-5" to="/about">
            About
          </Link>
          <Link className="btn btn-primary m-5" to="/product">
            Product
          </Link>
          <Link className="btn btn-primary m-5" to="/price">
            Price
          </Link>
          <Link className="btn btn-primary m-5" to="/contact">
            Contact
          </Link>
        </nav>
      </div>
    );
  }
}
