import React, { Component } from "react";

export default class Not extends Component {
  render() {
    return <h1 className="display-1 text-danger text-center">404 Not Found</h1>;
  }
}
